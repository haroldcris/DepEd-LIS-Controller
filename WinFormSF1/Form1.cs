﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LisController.Library;
using LisController.Sf1;
using LisController.Enum;
using LisController.Sf5;
using Record = LisController.Sf1.Record;


namespace WinFormSF1
{
	public partial class Form1 : Form
	{
		readonly Downloader _Sf1Downloader = new Downloader();
		public Form1()
		{
			InitializeComponent();
			
		}

		private void btnLoadSections_Click(object sender, EventArgs e)
		{
			using (new MouseWaitCursor())
			{
				var list = _Sf1Downloader.GetSectionsWithLink( K12GradeLevel.Grade7);
				
				//LISController.
				

				lstSections.Items.Clear();
				foreach (var i in list)
				{
					lstSections.Items.Add(i);
				}
			}
		}

		private async void button1_Click(object sender, EventArgs e)
		{
			try
			{
				foreach (SectionLink item in lstSections.CheckedItems)
				{
					await ParseSection(item.Url + @"&status=6", "TRANSFERRED_OUT");
					await ParseSection(item.Url + @"&status=7", "DROPPED_OUT");
					await ParseSection(item.Url + @"&status=5", "NO_LONGER_IN_SCHOOL");
					await ParseSection(item.Url);

					lblStatus.Text = "Sleeping for a second...";
					System.Threading.Thread.Sleep(2000);
					Console.WriteLine("Resuming...");
					
				}
				FlashWindow.Flash(this);
				MessageBox.Show("Done");
			}
			catch (Exception ex)
			{
				FlashWindow.Flash(this);
				MessageBox.Show(ex.ToString());
			}
		}


		private async Task ParseSection(string sectionLink, string Status = "")
		{
			string FileName = @"D:\lis.txt";

			Dictionary<string, string> hrefs;
			Console.WriteLine("Parsing " + sectionLink);
			lblStatus.Text = "Loading Page... " + sectionLink;
			this.Refresh();

			try
			{
				hrefs = await Task.Run(() => _Sf1Downloader.GetStudentUrlFromSection(sectionLink));
			}
			catch (Exception )
			{
				throw;
			}

			lblStatus.Text = "Page Loaded";
			this.Refresh();

			if (hrefs == null)
			{
				lblStatus.Text = "Error......";
				return;
			}

			List<Task<Record>> taskList = new List<Task<Record>>();
			foreach (KeyValuePair<string, string> item in hrefs)
			{
				lblStatus.Text = "Parsing " + item.Key;
				this.Refresh();
				Task<Record> t = Task.Run(() => _Sf1Downloader.GetSf1Record(item.Value));
				taskList.Add(t);
			}

			StringBuilder str = new StringBuilder();
			str.Append("Grade")
				.Append("\tSection")
				.Append("\tLRN")
				.Append("\tLastname")
				.Append("\tFirstname")
				.Append("\tMiddlename")
				.Append("\tExtension")
				.Append("\tGender")
				.Append("\tBirthDate")
				.Append("\tReligion")
				.Append("\tLanguage")
				.Append("\tMother")
				.Append("\tFather")
				.Append("\tLRNID")
				.Append("\tStatus")
				.AppendLine();

			if (!File.Exists(FileName))
			{
				using (var sw = new StreamWriter(FileName, true))
				{
					sw.Write(str.ToString());
				}
			}

			rtb.Text = str.ToString();


			progress.Maximum = taskList.Count;
			progress.Value = 0;
			while (taskList.Count > 0)
			{
				progress.Value += 1;
				Task<Record> tStudent = await Task.WhenAny(taskList.ToList());
				taskList.Remove(tStudent);
				Record s = await tStudent;
				if (s != null)
				{
					//students.Add(s);
					str.Clear();
					str.Append(s.GradeLevel)
						.Append("\t" + s.Section)
						.Append("\t" + s.LRN)
						.Append("\t" + s.Lastname)
						.Append("\t" + s.Firstname)
						.Append("\t" + s.Middlename)
						.Append("\t" + s.ExtensionName)
						.Append("\t" + s.Gender)
						.Append("\t" + s.BirthDate.ToShortDateString())
						.Append("\t" + s.Religion)
						.Append("\t" + s.Language)
						.Append("\t" + s.Mother)
						.Append("\t" + s.Father)
						.Append("\t" + s.LRNId)
						.Append("\t" + Status)
						.AppendLine();

					rtb.Text += str.ToString();

					using (StreamWriter sw = new StreamWriter(FileName, true))
					{
						sw.Write(str);
					}

				}
			}
		}
	}
}
