﻿namespace WinFormSF1
{

	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.rtb = new System.Windows.Forms.RichTextBox();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
			this.progress = new System.Windows.Forms.ToolStripProgressBar();
			this.lstSections = new System.Windows.Forms.CheckedListBox();
			this.btnLoadSections = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(535, 9);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(65, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Parse";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// rtb
			// 
			this.rtb.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.rtb.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rtb.Location = new System.Drawing.Point(0, 0);
			this.rtb.Name = "rtb";
			this.rtb.Size = new System.Drawing.Size(435, 449);
			this.rtb.TabIndex = 1;
			this.rtb.Text = "";
			this.rtb.WordWrap = false;
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.progress});
			this.statusStrip1.Location = new System.Drawing.Point(0, 490);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(612, 22);
			this.statusStrip1.TabIndex = 4;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// lblStatus
			// 
			this.lblStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.lblStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
			this.lblStatus.Size = new System.Drawing.Size(39, 17);
			this.lblStatus.Text = "Ready";
			this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// progress
			// 
			this.progress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.progress.Name = "progress";
			this.progress.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
			this.progress.Size = new System.Drawing.Size(500, 16);
			this.progress.Step = 5;
			// 
			// lstSections
			// 
			this.lstSections.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstSections.FormattingEnabled = true;
			this.lstSections.Location = new System.Drawing.Point(0, 0);
			this.lstSections.Name = "lstSections";
			this.lstSections.Size = new System.Drawing.Size(149, 449);
			this.lstSections.TabIndex = 7;
			// 
			// btnLoadSections
			// 
			this.btnLoadSections.Location = new System.Drawing.Point(12, 9);
			this.btnLoadSections.Name = "btnLoadSections";
			this.btnLoadSections.Size = new System.Drawing.Size(93, 23);
			this.btnLoadSections.TabIndex = 8;
			this.btnLoadSections.Text = "Load Sections";
			this.btnLoadSections.UseVisualStyleBackColor = true;
			this.btnLoadSections.Click += new System.EventHandler(this.btnLoadSections_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(12, 38);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.lstSections);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.rtb);
			this.splitContainer1.Size = new System.Drawing.Size(588, 449);
			this.splitContainer1.SplitterDistance = 149;
			this.splitContainer1.TabIndex = 9;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(612, 512);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.btnLoadSections);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.RichTextBox rtb;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel lblStatus;
		private System.Windows.Forms.ToolStripProgressBar progress;
		private System.Windows.Forms.CheckedListBox lstSections;
		private System.Windows.Forms.Button btnLoadSections;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}

