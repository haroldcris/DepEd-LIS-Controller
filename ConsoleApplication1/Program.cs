﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using LisController.LrnVerifier;
using LisController.Sf5;

namespace ConsoleSF5
{
	static class Program
	{
		static void Main(string[] args)
		{
            var v = new LisController.LrnVerifier.LrnVerifier();
		    Task<LrnRecord> t = Task.Factory.StartNew(() => v.FindRecord("301064100009"));
            t.Wait();

		    var n = (LrnRecord) t.Result;

            if (n != null)
		    {
		        Console.WriteLine("\n\n\nLRN:\t{0}", n.LRN);
		        Console.WriteLine("Firstname:\t{0}", n.Firstname);
		        Console.WriteLine("Middlename:\t{0}", n.Middlename);
                Console.WriteLine("Lastname:\t{0}", n.Lastname);
                Console.WriteLine("Birthdate:\t{0}\n\n", n.BirthDate.ToString("yyyy MMMM dd"));

		    }
		    Console.WriteLine("Finished");
            
		}


	   

	    
		static void UploadAverage()
        {
            Uploader SF5 = new Uploader();

            var recordService = new LisController.ProMis.RecordService();
            recordService.LoadStudents();

            Console.WriteLine("Hello");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\LISLog.txt", true))
            {
                ICollection<Task> taskList = new List<Task>();

                foreach (var item in recordService.Items)
                {
                    var log = $"{item.Section} - {item.LRNId} - {item.StudentNumber} => {item.Fullname} -> {item.GeneralAverage}";

                    Console.WriteLine(DateTime.Now.ToString() + ": " + log);
                    file.WriteLine(DateTime.Now.ToString() + ": " + log);

                    //Task<bool> t = Task.Factory.StartNew(() => {
                    //    bool ret = SF5.UpdateStatus(item);
                    //    if (ret == false) WriteError(log);
                    //    return ret;
                    //});
                    //Console.WriteLine("Adding Task ");
                    	if (!SF5.UpdateStatus(item)) WriteError(log);
                    //taskList.Add(t);
                }

                //while (taskList.Count != 0)
                //{
                //    var task = Task.WhenAny(taskList);
                //    taskList.Remove(task);
                //}

                file.Close();
            }

            Console.WriteLine("Done.....");
            Console.ReadKey();
        }

        static void WriteError(string log)
		{
			using (System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\LIS_ErrorLog.txt", true))
			{
				file.WriteLineAsync(log);
				file.Close();
			}
		}
	}
}