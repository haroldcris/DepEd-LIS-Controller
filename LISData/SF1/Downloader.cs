﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LisController.Enum;
using LisController.Sf5;

namespace LisController.Sf1
{
	public class Downloader: Base
	{
		List<Record> ListOfStudents = new List<Record>();

		public Dictionary<string, string> GetStudentUrlFromSection (string sectionUrl)
		{
			Console.WriteLine("Parsing " + sectionUrl);
			//string html = await _WebClient.DownloadStringTaskAsync ( new Uri(_credential.ServerUrl + sectionUrl));
			string html = _WebClient.DownloadString(new Uri(_credential.ServerUrl + sectionUrl));

			Console.WriteLine("Done Parsing " + sectionUrl);


			var listOfUrls = new Dictionary<string, string>();
			var h = new HtmlAgilityPack.HtmlDocument();
			h.LoadHtml(html);

			HtmlNode htmlList = h.DocumentNode.Descendants().FirstOrDefault(x => (x.Name == "table" && x.Attributes["class"].Value == "table table-bordered table-condensed table-striped table-sticky"));

			if (htmlList == null) return null;
			htmlList = htmlList.Descendants().FirstOrDefault(x => x.Name == "tbody");

			foreach (HtmlNode n in htmlList.Descendants().Where(x => x.Name == "tr").ToList())
			{
				HtmlNodeCollection nodes = n.ChildNodes;

				if (nodes.Count >= 4)
				{
					string num = "", lrn = "", name = "", href = "";

					if (nodes[1] != null)
					{
						if (nodes[1].Attributes["class"] != null &&
							nodes[1].Attributes["class"].Value == "text-right")
							num = nodes[1].InnerText.ToString();
					}

					if (nodes[3] != null)
					{
						lrn = nodes[3].SelectSingleNode("span").InnerText;
						name = nodes[3].ChildNodes[2].InnerText.Substring(1);
					}


					if (nodes.Count == 4)
					{
						Console.Write("NOLRN");
						href = nodes[3].ChildNodes[9].ChildNodes[0].Attributes["href"].Value;
						lrn = href;
					}
					else {
						if (nodes[11] != null)
						{
							href = nodes[11].ChildNodes[0].Attributes["href"].Value;
						}
					}


					if (listOfUrls.ContainsKey(lrn))
					{
						Console.WriteLine("Duplicate LRN " + lrn);
						using (StreamWriter sw = new StreamWriter(@"D:\DuplicateLRN.txt", true))
						{
							sw.Write(string.Format("{0}\t{1}\n", lrn, name));
						}
						listOfUrls.Add(lrn + "x", href);
						continue;
					}

					//Console.WriteLine("\t\t\tParsing " + sectionUrl + "=> " + lrn);
					listOfUrls.Add(lrn, href);
				}
			}

			return listOfUrls;
		}

		public IEnumerable<SectionLink> GetSectionsWithLink(K12GradeLevel gradeLevel)
		{
			ICollection<SectionLink> listOfSections= new List<SectionLink>();
			
			var gradeLabel = "";
			switch (gradeLevel)
			{
				case K12GradeLevel.Grade7: gradeLabel = "Grade 7 (Year I)"; break;
				case K12GradeLevel.Grade8: gradeLabel = "Grade 8 (Year II)"; break;
				case K12GradeLevel.Grade9: gradeLabel = "Grade 9 (Year III)"; break;
				case K12GradeLevel.Grade10: gradeLabel = "Grade 10 (Year IV)"; break;
			}

			string html = Library.Retry.Do( ()=> _WebClient.DownloadString(_credential.ServerUrl + "/enrolment"), TimeSpan.FromSeconds(1),2);

			HtmlAgilityPack.HtmlDocument h = new HtmlDocument();
			h.LoadHtml(html);
			for (var counter = 0; counter < 1; counter++)
			{
				if (h.DocumentNode.Descendants().ToString().Contains("Please sign in"))
				{
					Init();
					if (counter > 0) throw new Exception("Log In Required");
				}
			}

			HtmlNode g = h.DocumentNode.Descendants().FirstOrDefault(x => (x.Name == "div" &&
																  x.Attributes.Contains("class") &&
																  x.Attributes["class"].Value == "panel-heading" &&
																  x.InnerText == gradeLabel));
			
			List<string> col = new List<string>();

			foreach (HtmlNode link in g.ParentNode.Descendants("a"))
			{
				string href = link.GetAttributeValue("href", string.Empty);
				string hrefLabel = link.InnerText.ToString();

				//Console.WriteLine(href);
				if (href.Contains("/enrolment/masterlist/"))
				{
					if (listOfSections.All(item => item.Url != href))
					{
						SectionLink sectionLink = new SectionLink();
						sectionLink.Name = hrefLabel;
						sectionLink.Url = href;

						listOfSections.Add(sectionLink);
					}
				}

			}

			return listOfSections;
		}

		public async Task<Record> GetSf1Record(string studentUrl)
		{
			Console.WriteLine("parsing " + studentUrl);
			string html = await DownloadPageAsync(_credential.ServerUrl + studentUrl);
			Console.WriteLine("done parsing " + studentUrl);

			Record student = new Record();

			HtmlAgilityPack.HtmlDocument h = new HtmlDocument();
			h.LoadHtml(html);

			HtmlNode sectionNode = h.DocumentNode.Descendants().FirstOrDefault(x => x.Name == "ol");

			if (sectionNode != null)
			{
				student.Section = sectionNode.ChildNodes[3].InnerText.Split('-')[1].ToString().Trim();
				student.GradeLevel = sectionNode.ChildNodes[3].InnerText.Split('(')[0].ToString().Trim();
			}

			HtmlNode node = h.DocumentNode.Descendants().FirstOrDefault(x => x.Name == "dt" && x.InnerText == "LRN");
			if (node == null) return null;

			HtmlNode parentNode = node.ParentNode; //7 Lastname 11 15  19-G 23

			student.LRN = parentNode.ChildNodes[3].InnerText.Trim();
			student.Lastname = parentNode.ChildNodes[7].InnerText;
			student.Firstname = parentNode.ChildNodes[11].InnerText;
			student.Middlename = parentNode.ChildNodes[15].InnerText;

			if (parentNode.ChildNodes[15].InnerText == "NO MIDDLE")
			{
				student.Middlename = " ";
			}
			student.ExtensionName = "";

			if (parentNode.ChildNodes[17].InnerText == "Extensionname")
			{
				student.ExtensionName = parentNode.ChildNodes[19].InnerText.Trim();
				student.Gender = parentNode.ChildNodes[23].InnerText;
				student.BirthDate = DateTime.Parse(parentNode.ChildNodes[27].InnerText.ToString().TrimStart().Substring(0, 10));
				student.Mother = parentNode.ChildNodes[31].InnerText;

			}
			else {
				student.Gender = parentNode.ChildNodes[19].InnerText;
				student.BirthDate = DateTime.Parse(parentNode.ChildNodes[23].InnerText.ToString().TrimStart().Substring(0, 10));
				student.Mother = parentNode.ChildNodes[27].InnerText;
			}

			//father
			student.Father = parentNode.ParentNode.ChildNodes[5].ChildNodes[3].InnerText;
			student.Religion = parentNode.ParentNode.ChildNodes[7].ChildNodes[7].InnerText;
			student.Language = parentNode.ParentNode.ChildNodes[7].ChildNodes[11].InnerText;

			student.LRNId = studentUrl.Substring(studentUrl.LastIndexOf("/", StringComparison.Ordinal) + 1);
			return student;
		}

		private async Task<string> DownloadPageAsync(string URL)
		{
			string strResponse;

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(URL);
			webRequest.Method = "GET";
			webRequest.CookieContainer = _WebClient.CookieContainer;

			using (StreamReader stIn = new StreamReader(webRequest.GetResponse().GetResponseStream()))
			{
				strResponse = await stIn.ReadToEndAsync();
				stIn.Close();
			}
			return strResponse;
		}


		//private string downloadPage(string URL)
		//{
		//	try
		//	{
		//		string strResponse;
		//		//if (URL == "") URL = "http://lis.deped.gov.ph/enrolment/learner/101291711";
		//		//CookieContainer cookieJar = new CookieContainer();

		//		Cookie cookie = new Cookie("deped_lis", cookie_lis);
		//		Cookie cookie2 = new Cookie("deped_uis", cookie_uis);

		//		if (myCookies == null)
		//		{
		//			myCookies = new CookieContainer();
		//			myCookies.Add(new Uri(URL), cookie);
		//			myCookies.Add(new Uri(URL), cookie2);
		//		}

		//		HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(URL);
		//		webRequest.Method = "GET";
		//		webRequest.CookieContainer = myCookies;

		//		WebResponse webResponse = webRequest.GetResponse();
		//		using (StreamReader stIn = new StreamReader(webResponse.GetResponseStream()))
		//		{
		//			strResponse = stIn.ReadToEnd();
		//		}

		//		CookieCollection cookies = myCookies.GetCookies(new Uri(URL));

		//		if (cookies.Count > 0)
		//		{
		//			myCookies = new CookieContainer();
		//			foreach (Cookie c in cookies)
		//			{
		//				myCookies.Add(c);
		//				Console.WriteLine(c.Name + ": " + c.Value);
		//			}
		//		}
		//		return strResponse;

		//	}
		//	catch (Exception ex)
		//	{
		//		throw new Exception("Error in DownloadPage", ex);
		//	}
		//}
	}

}
