﻿using System;

namespace LisController.Sf1
{
	public class Record
	{
		public string LRN { get; set; }
		public string LRNId { get; set; }
		public string Lastname { get; set; }
		public string Firstname { get; set; }
		public string Middlename { get; set; }
		public string ExtensionName { get; set; }
		public string Gender { get; set; }
		public DateTime BirthDate { get; set; }

		public string Mother { get; set; }
		public string Father { get; set; }

		public string Section { get; set; }
		public string GradeLevel { get; set; }
		public string Language { get; set; }
		public string Religion { get; set; }
	}
}
