﻿namespace LisController.Sf5
{
	public class SectionLink
	{
		public string Name { get; set; }
		public string Url { get; set; }
		public override string ToString()
		{
			return this.Name;
		}
	}


}