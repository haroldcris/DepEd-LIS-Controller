﻿using System;
using System.Collections.Specialized;
using System.Text;
using HtmlAgilityPack;
using LisController.Enum;

namespace LisController.Sf5
{
	public class Uploader : Base
	{
		private string _validationLogFile;

		public Uploader(string ErrorLogFilename = "D:\\LIS_ValidationErrorLog.txt")
		{
			_validationLogFile = ErrorLogFilename;
		}

		public bool UpdateStatus(Sf5.Record record)
		{
			if (record.GeneralAverage < 65) record.GeneralAverage = 65;

			var url = $"{_credential.ServerUrl}/enrolment/formal/{record.LRNId}/status";
			var ret = Library.Retry.Do(() => _WebClient.DownloadString(url), TimeSpan.FromSeconds(1), 2);
			var doc = new HtmlDocument();
			doc.LoadHtml(ret);
			var hNode = doc.GetElementbyId("formal_status__token");

			if (hNode == null)
			{
				Init();
				ret = _WebClient.DownloadString(url);
				doc = new HtmlDocument();
				doc.LoadHtml(ret);
				hNode = doc.GetElementbyId("formal_status__token");
				if (hNode == null && ret.Contains("Enrolment Status"))
				{
					Console.WriteLine(" =>  By Passed");
					return true;
				}
			}

			var token = hNode.Attributes["Value"].Value;

			var data = new NameValueCollection();

			switch (record.RecordStatus)
			{
				case RecordStatus.ClearAll:
					data = new NameValueCollection()
					{
						{"formal_status[statusId]", "0"},
						{"formal_status[generalAve]", ""},
						{"formal_status[_token]", token},
						{"formal_status[submit]", ""}
					};
					break;

				case RecordStatus.Promoted:

					data = new NameValueCollection()
					{
                        {"formal_status[statusId]", "0"},
						{"formal_status[generalAve]", record.GeneralAverage.ToString()},                        
                        {"formal_status[_token]", token},
						{"formal_status[submit]", ""}
					};

                    if (record.IsHonorStudent) 
                        data.Add (new NameValueCollection() { { "formal_status[isHonor]", "1" } });

					var promotedRecord = record as PromotedRecord;
					if (promotedRecord != null && promotedRecord.IsHonor)
						data.Add(new NameValueCollection() {{"formal_status[isHonor]", "1"}});
					break;


				case RecordStatus.Retained:

					data = new NameValueCollection()
					{
						{"formal_status[statusId]", "2"},
						{"formal_status[generalAve]", record.GeneralAverage.ToString()}, 
						{"formal_status[_token]", token},
						{"formal_status[submit]", ""}
					};
					break;


				case RecordStatus.Conditional:
					data = new NameValueCollection()
					{
						{"formal_status[statusId]", "8"},
						{"formal_status[generalAve]", record.GeneralAverage.ToString()},
						{"formal_status[_token]", token},
						{"formal_status[submit]", ""}
					};
					break;

				case RecordStatus.Dropped:
					var droppedRecord = record as DroppedRecord;
					if (droppedRecord != null)
						data = new NameValueCollection()
						{
							{"formal_status[statusId]", "5"},
							{"formal_status[droppedAt]", droppedRecord.AsOfDate.ToString("yyyy-MM-dd")},
							{"formal_status[droppedReasonId]", droppedRecord.ReasonCode},
							{"formal_status[_token]", token},
							{"formal_status[submit]", ""}
						};
					break;

				case RecordStatus.Transferred:
					var transferredRecord = record as DroppedRecord;
					if (transferredRecord != null)
						data = new NameValueCollection()
						{
							{"formal_status[statusId]", "5"},
							{"formal_status[droppedAt]", transferredRecord.AsOfDate.ToString("yyyy-MM-dd")},
							{"formal_status[droppedReasonId]", transferredRecord.ReasonCode},
							{"formal_status[_token]", token},
							{"formal_status[submit]", ""}
						};
					break;
			}


			//var res = _WebClient.UploadValues($"{HomeUrl}/enrolment/formal/{record.LrnId}/status", data);
			byte[] bytes = Library.Retry.Do(
								() => _WebClient.UploadValues($"{_credential.ServerUrl}/enrolment/formal/{record.LRNId}/status", data),
								TimeSpan.FromSeconds(1), 2);

			var strRes = Encoding.Default.GetString(bytes);

			if (strRes.Contains("Form validation failed"))
			{
				WriteError($"{record.Section} - {record.Fullname} - Validation Error");
				Console.WriteLine($"{record.Section} - {record.Fullname} - Validation Error");
				//throw new Exception("Validation Error");
			}

			if (strRes.Contains("Status updated.")) return true; //Console.WriteLine("SUCCESSFUL......");

			return false;
		}


		void WriteError(string log)
		{
			using (System.IO.StreamWriter file = new System.IO.StreamWriter(_validationLogFile, true))
			{
				file.WriteLineAsync(log);
				file.Close();
			}
		}
	}
}