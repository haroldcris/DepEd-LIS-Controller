﻿using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Text;



namespace LisController
{
	public abstract class Base
    {
	    protected Credential _credential;
		protected readonly MyWebBrowser _WebClient = new MyWebBrowser();

		protected void Init()
        {
			_credential = new Credential();

			var homeUrl = _credential.ServerUrl;
	        var userName = _credential.Username;
	        var password = _credential.Password;

            //Console.WriteLine($"using {homeUrl} - {userName}");
            using (MyWebBrowser client = new MyWebBrowser())
            {
                Console.Write("Downloading Log On Screen from {0}...",homeUrl);
                var html = Library.Retry.Do(()=> client.DownloadString(homeUrl),TimeSpan.FromSeconds(1),2);
                Console.WriteLine("Done.");

                //Return if Logged In
                if (html.Contains("Sign out")) return;

                var data = new NameValueCollection
                {
                    {"_username", userName},
                    {"_password", password}
                };
                
                Console.Write("Trying to Login...");
                var webResponse = client.UploadValues(homeUrl + "/uis/login_check", data);
                _WebClient.CookieContainer = client.CookieContainer;

                html = Encoding.Default.GetString(webResponse);

                if (html.Contains("Please sign in")) throw new AuthenticationException("Invalid Username or Password");
				Console.WriteLine("Logged In.");
			}
        }

        protected Base()
        {
            Init();
        }
    }
}
