﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using LisController.Sf1;

namespace LisController.LrnVerifier
{
    public class LrnVerifier:Base
    {
        public LrnRecord FindRecord(string lrn)
        {
            Task<LrnRecord> ret = Task.Factory.StartNew( () =>
            {
                HtmlNode.ElementsFlags.Remove("form");

                Console.Write("Downloading Form...");
                var response = this._WebClient.DownloadString("http://111.125.94.99/shs/enrollment");
                Console.WriteLine("Done");
                var doc = new HtmlDocument();
                    doc.LoadHtml(response);
                var hNodes = doc.DocumentNode.Descendants("input");
                var hNode = hNodes.FirstOrDefault(n => n.Id == "enroll_senior_form__token");

                var token = hNode?.Attributes["Value"].Value;

                var data = new NameValueCollection()
                    {
                        {"enroll_senior_form[lrn]", lrn},
                        {"formal_status[_token]", token},
                    };

                byte[] bytes = Library.Retry.Do(() => this._WebClient.UploadValues("http://111.125.94.99/shs/enrollment", data) 
                                    , TimeSpan.FromSeconds(1), 2);

                response = Encoding.Default.GetString(bytes);
                doc.LoadHtml(response);
                var hforms = doc.DocumentNode.Descendants("form");
                var hTable = hforms.ElementAtOrDefault(0);
                hNode = hTable.Descendants("tr").LastOrDefault();

                if (hNode == null) return null;

                var record = new LrnRecord();

                record.LRN = hNode.ChildNodes[3].ChildNodes[0].InnerText;
                record.Firstname = hNode.ChildNodes[5].ChildNodes[0].InnerText;
                record.Middlename = hNode.ChildNodes[7].ChildNodes[0].InnerText;
                record.Lastname = hNode.ChildNodes[9].ChildNodes[0].InnerText;
                record.ExtensionName = hNode.ChildNodes[11].ChildNodes[0].InnerText;
                record.Gender = hNode.ChildNodes[13].ChildNodes[0].InnerText;
                record.BirthDate = DateTime.Parse(hNode.ChildNodes[15].ChildNodes[0].InnerText);

                //Console.WriteLine(hNode.ChildNodes[3].ChildNodes[0].InnerText); //LRN
                //Console.WriteLine(hNode.ChildNodes[5].ChildNodes[0].InnerText);  //Firstname
                //Console.WriteLine(hNode.ChildNodes[7].ChildNodes[0].InnerText);//Middlename
                //Console.WriteLine(hNode.ChildNodes[9].ChildNodes[0].InnerText); //Lastname
                //Console.WriteLine(hNode.ChildNodes[11].ChildNodes[0].InnerText);
                //Console.WriteLine(hNode.ChildNodes[13].ChildNodes[0].InnerText); //Gender
                //Console.WriteLine(hNode.ChildNodes[15].ChildNodes[0].InnerText); //Bdate
                return record;
            });
            ret.Wait();
            Console.WriteLine("Waiting for Reply...");
            return ret.Result;
        }
    }
}
