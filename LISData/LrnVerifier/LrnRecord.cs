﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisController.LrnVerifier
{
    public class LrnRecord
    {
        public string LRN { get; set; }
        public string LRNId { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string ExtensionName { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }

    }
}
