﻿namespace LisController.Enum
{
	/// <summary>
	/// For SF5 Uploader
	/// </summary>
	public enum RecordStatus
	{
		Dropped,
		Transferred,
		Promoted,
		Retained,
		Conditional,
		ClearAll
	}



	/// <summary>
	/// For SF1
	/// </summary>
	public enum K12GradeLevel
	{
		Grade7,
		Grade8,
		Grade9,
		Grade10
	}

	
}