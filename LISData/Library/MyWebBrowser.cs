﻿using System;
using System.Net;

namespace LisController
{
    public class MyWebBrowser : WebClient
    {
        
        public CookieContainer CookieContainer { get; set; }

        internal MyWebBrowser()
        {
            CookieContainer = new CookieContainer();

            // Makes Download Faster
            // Taken from http://stackoverflow.com/questions/6988981/webclient-is-very-slow
            this.Proxy = null;
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = CookieContainer;
            }
            return request;
        }
    }
}
