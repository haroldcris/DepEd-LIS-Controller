﻿using System;
using System.Windows.Forms;

namespace LisController.Library
{
	public class MouseWaitCursor : IDisposable
	{
		Cursor _oldCursor = null;
		public MouseWaitCursor()
		{
			_oldCursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
		}

		public void Dispose()
		{
			Cursor.Current = _oldCursor;
		}
	}
}
